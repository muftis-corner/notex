import 'dart:io';

import 'package:flutter_test/flutter_test.dart';
import 'package:hive/hive.dart';
import 'package:notes/page/notes/model/notes.dart';

void initialiseHive() async {
  var path = Directory.current.path;
  Hive
    ..init(path)
    ..registerAdapter(NoteDataAdapter());

  //Always starts from a clean box
  Hive.deleteBoxFromDisk('test_box');
}

void main() async {
  initialiseHive();

  final newCard = NoteData(
      'Note Test',
      [
        {"key": "value"},
        {"key": "value"},
      ],
      DateTime.now());

  group('Given a real loyalty cards repository instance and a real Hive box',
      () {
    test('should save and then retrieve saved loyalty card from the repository',
        () async {
      final loyaltyCardRepository = NameCardRepository(NameCardBox());

      await loyaltyCardRepository.save(newCard);

      expect(await loyaltyCardRepository.getAll(), [newCard]);
    });
  });
}
