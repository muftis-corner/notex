class WordCutter {
  static cut(String word, int maxLength) {
    if (word.length > maxLength) {
      String res = word.substring(0, maxLength);
      if (res.endsWith(' ') || res.endsWith('.')) {
        return res;
      }
      return res + '...';
    }
    return word;
  }
}
