import 'package:flutter/material.dart';

class ConfirmDialog extends StatelessWidget {
  final String? title;
  const ConfirmDialog({Key? key, this.title}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      title: title != null
          ? Center(
              child: Text(
                title!,
                style: TextStyle(fontSize: 16),
              ),
            )
          : Container(),
      content: Row(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: [
          ElevatedButton(
            onPressed: () => Navigator.pop(context, true),
            child: Text('Yes'),
          ),
          ElevatedButton(
            child: Text('No'),
            onPressed: () => Navigator.pop(context, false),
          )
        ],
      ),
    );
  }
}
