import 'package:notes/dependencies.dart';
import 'package:notes/page/account/model/account.dart';
import 'package:notes/page/checklist/model/checklist.dart';
import 'package:notes/page/checklist/model/checklistGroup.dart';
import 'package:notes/page/notes/model/notes.dart';

class HiveHelper {
  static Future<void> openAllBox() async {
    await Hive.openBox<NoteData>(HiveBox.note);
    await Hive.openBox<ChecklistGroupData>(HiveBox.checklist);
    await Hive.openBox<NoteData>(HiveBox.account);
  }

  static void registerAdapter() {
    Hive.registerAdapter(NoteDataAdapter());
    Hive.registerAdapter(ChecklistDataAdapter());
    Hive.registerAdapter(ChecklistGroupDataAdapter());
    Hive.registerAdapter(AccountDataAdapter());
  }
}
