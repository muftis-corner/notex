import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:notes/dependencies.dart';
import 'package:notes/page/notes/controller/notes.controller.dart';
import 'package:notes/page/notes/model/notes.dart';

class ControlDialog extends StatefulWidget {
  final int hiveKey;
  final NoteData noteData;
  const ControlDialog({Key? key, required this.hiveKey, required this.noteData})
      : super(key: key);

  @override
  _ControlDialogState createState() => _ControlDialogState();
}

class _ControlDialogState extends State<ControlDialog> {
  late BehaviorSubject<bool> favorite;
  late BehaviorSubject<bool> deleteState;

  @override
  void initState() {
    favorite = BehaviorSubject<bool>();
    favorite.add(widget.noteData.favorite);

    deleteState = BehaviorSubject<bool>();
    deleteState.add(false);

    super.initState();
  }

  @override
  void dispose() {
    favorite.close();
    deleteState.close();
    super.dispose();
  }

  Widget deleteWidget() {
    return StreamBuilder(
        stream: deleteState.stream,
        builder: (context, AsyncSnapshot<bool> snapshot) {
          if (snapshot.hasData && snapshot.data == true) {
            return Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              mainAxisSize: MainAxisSize.max,
              children: [
                ElevatedButton(
                    onPressed: () {
                      NotesController().delete(widget.hiveKey);
                      return Navigator.pop(context);
                    },
                    child: Text('Yes')),
                ElevatedButton(
                    onPressed: () {
                      deleteState.add(false);
                    },
                    child: Text('No')),
              ],
            );
          } else {
            return ListTile(
              title: Text('Delete'),
              enableFeedback: true,
              trailing: Icon(Ionicons.trash_outline),
              onTap: () {
                deleteState.add(true);
              },
            );
          }
        });
  }

  Widget favoriteWidget() {
    return ListTile(
      title: Text('Favorite'),
      enableFeedback: true,
      trailing: starIcon(),
      onTap: () {
        favorite.add(!widget.noteData.favorite);
        NotesController().setFav(widget.hiveKey, widget.noteData);
      },
    );
  }

  Widget starIcon() {
    return StreamBuilder(
        stream: favorite.stream,
        builder: (context, AsyncSnapshot<bool> snapshot) {
          if (snapshot.hasData) {
            if (snapshot.data!) {
              return Icon(Ionicons.star, color: Colors.amber);
            }
          }
          return Icon(Ionicons.star_outline, color: Colors.grey);
        });
  }

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
        content: Column(
      mainAxisSize: MainAxisSize.min,
      children: [favoriteWidget(), deleteWidget()],
    ));
  }
}
