import 'package:flutter/material.dart';
import 'package:notes/dependencies.dart';
import 'package:notes/page/dashboard/data/CreateNewItem.dart';

class CreateNewDialog extends StatelessWidget {
  const CreateNewDialog({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      title: Text(Label.createNew),
      content: Column(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: createNewItem
              .map((item) => Container(
                    margin: EdgeInsets.only(bottom: 4),
                    decoration: BoxDecoration(
                        border: Border.all(color: Colors.grey),
                        borderRadius: BorderRadius.circular(5)),
                    child: ListTile(
                      leading: item.icon,
                      title: Text(item.label),
                      onTap: () {
                        Navigator.pop(context);
                        Navigator.pushNamed(context, item.route!);
                      },
                    ),
                  ))
              .toList()),
    );
  }
}
