import 'package:flutter/material.dart';
import 'package:notes/dependencies.dart';
import 'package:notes/page/dashboard/controller/view.controller.dart';
import 'package:notes/page/dashboard/data/DashboardItem.dart';
import 'package:notes/page/notes/model/notes.dart';

class DashboardView extends StatefulWidget {
  const DashboardView({Key? key}) : super(key: key);

  @override
  _DashboardViewState createState() => _DashboardViewState();
}

class _DashboardViewState extends State<DashboardView> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: DashboardViewController.createAppBar(),
        body: DashboardViewController.createBody(context),
        floatingActionButton: DashboardViewController.createFAB(context),
        floatingActionButtonLocation:
            FloatingActionButtonLocation.miniCenterDocked,
        bottomNavigationBar: DashboardViewController.createBNB(
            StreamManager.uiSubject[StreamKey.bottomNavbar]!));
  }
}
