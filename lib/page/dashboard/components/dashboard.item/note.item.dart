import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_quill/flutter_quill.dart' as Q;
import 'package:notes/page/dashboard/components/dialogs/ControlDialog.dart';
import 'package:notes/page/notes/components/main.dart';
import 'package:notes/page/notes/controller/notes.controller.dart';

import 'package:notes/page/notes/model/notes.dart';

import '../../../../dependencies.dart';

class NoteItemComponent extends StatefulWidget {
  final int hiveKey;
  final NoteData noteData;

  NoteItemComponent({
    Key? key,
    required this.noteData,
    required this.hiveKey,
  }) : super(key: key);

  @override
  _NoteItemComponentState createState() => _NoteItemComponentState();
}

class _NoteItemComponentState extends State<NoteItemComponent> {
  NotesController notesController = NotesController();

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onLongPress: () async {
        HapticFeedback.lightImpact();
        await showDialog(
            context: context,
            builder: (context) => ControlDialog(
                hiveKey: widget.hiveKey, noteData: widget.noteData));
      },
      onTap: () {
        Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) => NotesView(
                      hiveKey: widget.hiveKey,
                      noteData: widget.noteData,
                    )));
      },
      child: Container(
        padding: EdgeInsets.all(8),
        height: 100,
        width: 100,
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(10),
            border: Border.all(color: Colors.grey, width: 1.5)),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            if (widget.noteData.title.length > 1)
              Padding(
                padding: const EdgeInsets.only(bottom: 8.0),
                child: Text(
                  WordCutter.cut(widget.noteData.title, 30),
                  style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
                ),
              ),
            Text(
              WordCutter.cut(
                  Q.Document.fromJson(widget.noteData.documentJson)
                      .toPlainText(),
                  50),
              style: TextStyle(fontSize: 14, fontWeight: FontWeight.w400),
            ),
          ],
        ),
      ),
    );
  }
}
