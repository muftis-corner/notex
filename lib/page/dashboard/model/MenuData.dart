import 'package:flutter/material.dart';

class MenuData {
  String label;
  Icon icon;
  Icon? iconActive;
  Widget? widget;
  String? route;
  MenuData({
    required this.label,
    required this.icon,
    this.iconActive,
    this.widget,
    this.route,
  });
}
