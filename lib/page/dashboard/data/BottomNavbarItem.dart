import 'package:flutter/material.dart';
import 'package:notes/dependencies.dart';

import '../dashboard.dart';

// ignore: non_constant_identifier_names
List<MenuData> BNBItem = [
  MenuData(
      label: Label.note,
      icon: Icon(Ionicons.document_text_outline),
      iconActive: Icon(Ionicons.document_text)),
  MenuData(
      label: Label.list,
      icon: Icon(Ionicons.checkmark_circle_outline),
      iconActive: Icon(Ionicons.checkmark_circle)),
  // MenuData(
  //     label: Label.account,
  //     icon: Icon(Ionicons.lock_closed_outline),
  //     iconActive: Icon(Ionicons.lock_closed)),
  // MenuData(
  //     label: Label.search,
  //     icon: Icon(Ionicons.search_outline),
  //     iconActive: Icon(Ionicons.search)),
];
