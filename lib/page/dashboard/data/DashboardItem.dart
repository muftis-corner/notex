import 'package:flutter/widgets.dart';
import 'package:notes/page/dashboard/controller/view.controller.dart';

List<Widget> generateDashboardItem(BuildContext context) {
  return [
    DashboardViewController.generateNoteView(context),
    DashboardViewController.generateChecklistView(context),
    // DashboardViewController.generateAccountVaultView(context),
    // DashboardViewController.generateSearchView(context),
  ];
}
