import 'package:flutter/material.dart';
import '../../../dependencies.dart';
import '../dashboard.dart';

List<MenuData> createNewItem = [
  MenuData(
    label: Label.note,
    icon: Icon(Ionicons.text_outline),
    route: Routes.Note,
  ),
  MenuData(
      label: Label.checkList,
      icon: Icon(Ionicons.checkmark_circle_outline),
      route: Routes.Checklist),
  MenuData(label: Label.account, icon: Icon(Ionicons.lock_closed_outline)),
];
