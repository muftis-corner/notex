import 'package:notes/dependencies.dart';

class DashboardDataController {
  Box box;
  DashboardDataController(
    this.box,
  );

  dispose() {
    box.close();
  }
}
