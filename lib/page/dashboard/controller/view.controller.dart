import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:notes/page/checklist/components/main.dart';
import 'package:notes/page/dashboard/components/dialogs/CreateNewDialog.dart';
import 'package:notes/page/dashboard/data/BottomNavbarItem.dart';
import 'package:notes/page/dashboard/data/DashboardItem.dart';
import 'package:notes/page/notes/model/notes.dart';

import '../../../dependencies.dart';
import '../dashboard.dart';

class DashboardViewController {
  static Widget generateNoteView(BuildContext context) {
    Box<NoteData> noteBox = Hive.box(HiveBox.note);
    return Padding(
      padding: const EdgeInsets.all(4.0),
      child: ValueListenableBuilder(
        valueListenable: noteBox.listenable(),
        builder: (context, _, widget) => GridView.builder(
            itemCount: noteBox.values.length,
            gridDelegate:
                SliverGridDelegateWithFixedCrossAxisCount(crossAxisCount: 2),
            itemBuilder: (context, index) {
              return Padding(
                padding: const EdgeInsets.all(4.0),
                child: NoteItemComponent(
                  hiveKey: noteBox.keys.toList()[index],
                  noteData: noteBox.values.toList()[index],
                ),
              );
            }),
      ),
    );
  }

  static Widget generateChecklistView(BuildContext context) {
    Box<ChecklistGroupData> checklistBox = Hive.box(HiveBox.checklist);

    return Padding(
      padding: const EdgeInsets.all(4.0),
      child: ValueListenableBuilder(
        valueListenable: checklistBox.listenable(),
        builder: (context, _, widget) => ListView.builder(
            itemCount: checklistBox.values.length,
            itemBuilder: (context, index) {
              int hiveKey = checklistBox.keys.toList()[index];
              ChecklistGroupData item = checklistBox.values.toList()[index];
              return Padding(
                padding: const EdgeInsets.all(4.0),
                child: Container(
                  decoration: BoxDecoration(
                      border: Border.all(),
                      borderRadius: BorderRadius.circular(10)),
                  child: ListTile(
                      dense: true,
                      onTap: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                            builder: (context) => ChecklistView(
                              hiveKey: hiveKey,
                              data: item,
                            ),
                          ),
                        );
                      },
                      title: Text(item.title),
                      trailing: Text(item.items.length.toString())),
                ),
              );
            }),
      ),
    );
  }

  static Widget generateAccountVaultView(BuildContext context) {
    return Container(
      child: Text('AccountVault'),
    );
  }

  static Widget generateSearchView(BuildContext context) {
    return Container(
      child: Text('Search'),
    );
  }

  static FloatingActionButton createFAB(BuildContext context) {
    return FloatingActionButton(
      child: Icon(Ionicons.add),
      onPressed: () {
        HapticFeedback.mediumImpact();
        showDialog(
            barrierDismissible: true,
            context: context,
            builder: (context) => CreateNewDialog());
      },
    );
  }

  static Widget createBNB(BehaviorSubject behaviorSubject) {
    return StreamBuilder<dynamic>(
        stream: behaviorSubject.stream,
        builder: (context, snapshot) {
          int index = 0;
          if (snapshot.hasData) {
            index = snapshot.data!;
          }
          return BottomNavigationBar(
            showSelectedLabels: true,
            showUnselectedLabels: false,
            currentIndex: index,
            type: BottomNavigationBarType.fixed,
            onTap: (index) {
              behaviorSubject.add(index);
            },
            items: BNBItem.map(
              (item) => BottomNavigationBarItem(
                icon: item.icon,
                label: item.label,
                activeIcon: item.iconActive,
              ),
            ).toList(),
          );
        });
  }

  static PreferredSizeWidget createAppBar() {
    return AppBar(
      title: Text('NoteX'),
      centerTitle: true,
      leading: IconButton(
        icon: Icon(Ionicons.ellipsis_vertical_outline),
        onPressed: () {},
      ),
      actions: [
        IconButton(onPressed: () {}, icon: Icon(Ionicons.filter_circle_outline))
      ],
    );
  }

  static Widget createBody(BuildContext context) {
    return StreamBuilder<dynamic>(
      stream: StreamManager.uiSubject[StreamKey.bottomNavbar]!,
      builder: (context, snapshot) {
        int index = 0;
        if (snapshot.hasData) {
          index = snapshot.data!;
        }
        return generateDashboardItem(context)[index];
      },
    );
  }
}
