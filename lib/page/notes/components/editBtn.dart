import 'package:flutter/material.dart';
import 'package:notes/dependencies.dart';

class EditButton extends StatelessWidget {
  final BehaviorSubject<bool> subject;
  const EditButton(this.subject, {Key? key}) : super(key: key);

  void changeState() {
    if (subject.value) {
      subject.add(false);
    } else {
      subject.add(true);
    }
  }

  @override
  Widget build(BuildContext context) => StreamBuilder<bool>(
      stream: subject.stream,
      builder: (ctx, snapshot) {
        return IconButton(
            onPressed: changeState,
            icon: Icon(subject.value
                ? Ionicons.create_outline
                : Ionicons.checkmark_outline));
      });
}
