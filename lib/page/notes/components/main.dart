import 'package:flutter/material.dart';
import 'package:flutter_quill/flutter_quill.dart' as Q;
import 'package:notes/dependencies.dart';
import 'package:notes/page/notes/components/editBtn.dart';
import 'package:notes/page/notes/controller/notes.controller.dart';
import 'package:notes/page/notes/model/notes.dart';

class NotesView extends StatefulWidget {
  final int? hiveKey;
  final NoteData? noteData;
  NotesView({Key? key, this.hiveKey, this.noteData}) : super(key: key);

  @override
  NotesViewState createState() => NotesViewState();
}

class NotesViewState extends State<NotesView> {
  NoteData noteData = NoteData('', [], DateTime.now());
  late TextEditingController titleText;
  late ScrollController _scrollController;
  late FocusNode _notesFocus;
  late Size size;
  late NotesController notesController;
  late Q.QuillController _controller;
  late BehaviorSubject<bool> readMode = BehaviorSubject<bool>();

  @override
  void initState() {
    this.notesController = NotesController();
    this._notesFocus = FocusNode();
    this._scrollController = ScrollController();

    if (widget.noteData != null) {
      this.noteData = widget.noteData!;
      _controller = Q.QuillController(
        document: Q.Document.fromJson(this.noteData.documentJson),
        selection: TextSelection.collapsed(offset: 0),
      );
      readMode.add(true);
    } else {
      _controller = Q.QuillController.basic();
      readMode.add(false);
    }

    titleText = TextEditingController(text: this.noteData.title);
    super.initState();
  }

  @override
  void dispose() {
    this.titleText.dispose();
    this._notesFocus.dispose();
    this._scrollController.dispose();
    this.readMode.close();
    super.dispose();
  }

  Future<bool> save() async {
    var documentJson = _controller.document.toDelta().toJson();
    var res;
    if (widget.hiveKey == null) {
      noteData = NoteData(titleText.text, documentJson, DateTime.now());
      if (titleText.text.length > 0 ||
          documentJson[0]['insert'].toString().trim().length > 0) {
        res = await this.notesController.save(noteData);
      } else {
        res = true;
      }
    } else {
      NoteData old = noteData;
      if (titleText.text != noteData.title) noteData.setTitle(titleText.text);
      if (documentJson != noteData.documentJson)
        noteData.setDocument(documentJson);
      res = await this.notesController.update(widget.hiveKey!, old, noteData);
    }
    StreamManager.uiSubject[StreamKey.bottomNavbar]!.add(0);
    if (res) Navigator.pop(context);
    return res;
  }

  Widget editor() {
    try {
      return Q.QuillEditor(
          textCapitalization: TextCapitalization.sentences,
          readOnly: readMode.value,
          controller: _controller,
          scrollController: _scrollController,
          autoFocus: true,
          focusNode: _notesFocus,
          onLaunchUrl: (url) {
            print(url);
          },
          scrollable: true,
          expands: true,
          placeholder: "What's in your mind ?",
          enableInteractiveSelection: true,
          padding: EdgeInsets.all(8),
          paintCursorAboveText: true,
          showCursor: !readMode.value);
    } catch (e) {
      return Container();
    }
  }

  @override
  Widget build(BuildContext context) {
    this.size = MediaQuery.of(context).size;
    return WillPopScope(
        onWillPop: save,
        child: StreamBuilder<Object>(
            stream: readMode,
            builder: (context, snapshot) {
              return Scaffold(
                appBar: AppBar(
                  title: TextField(
                    readOnly: readMode.value,
                    maxLines: 1,
                    controller: this.titleText,
                    style: TextStyle(fontSize: 24, color: Colors.white),
                    decoration: InputDecoration(
                        hintText: 'Untitled', border: InputBorder.none),
                  ),
                  actions: [EditButton(readMode)],
                ),
                bottomSheet: !readMode.value
                    ? SingleChildScrollView(
                        scrollDirection: Axis.horizontal,
                        child: Q.QuillToolbar.basic(
                          controller: _controller,
                          showCamera: true,
                        ),
                      )
                    : null,
                body: SafeArea(
                  child: Container(
                      padding: EdgeInsets.all(8),
                      height: size.height,
                      width: size.width,
                      child: Column(
                        children: [
                          Expanded(child: editor()),
                        ],
                      )),
                ),
              );
            }));
  }
}
