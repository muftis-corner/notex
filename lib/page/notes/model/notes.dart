import '../../../dependencies.dart';

part 'notes.g.dart';

@HiveType(typeId: 1)
class NoteData extends HiveObject {
  @HiveField(0)
  String title;
  @HiveField(1)
  List<dynamic> documentJson;
  @HiveField(2)
  DateTime createdAt;
  @HiveField(3)
  bool favorite;
  @HiveField(4)
  bool isDeleted;

  NoteData(
    this.title,
    this.documentJson,
    this.createdAt, {
    this.favorite = false,
    this.isDeleted = false,
  });

  void setFavorite(bool value) {
    this.favorite = value;
  }

  void setNewDate() {
    this.createdAt = new DateTime.now();
  }

  void setTitle(String value) {
    this.title = value;
  }

  void setDocument(List<dynamic> value) {
    this.documentJson = value;
  }

  void softDelete() {
    this.isDeleted = true;
  }

  void undelete() {
    this.isDeleted = false;
  }

  @override
  int get hashCode {
    return title.hashCode ^
        documentJson.hashCode ^
        createdAt.hashCode ^
        favorite.hashCode ^
        isDeleted.hashCode;
  }
}
