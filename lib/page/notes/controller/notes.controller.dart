import 'package:flutter/material.dart';

import 'package:notes/page/notes/model/notes.dart';

import '../../../dependencies.dart';

class NotesController {
  late Box box;

  NotesController() {
    this.box = Hive.box<NoteData>(HiveBox.note);
  }

  Future<bool> save(NoteData noteData) async {
    noteData.setNewDate();
    this.box.add(noteData).catchError((err) => throw false);
    Fluttertoast.showToast(msg: 'Note Created');
    return true;
  }

  Future<bool> update(
      int hiveKey, NoteData oldNoteData, NoteData noteData) async {
    print('functionupdate');
    noteData.setNewDate();
    await this.box.put(hiveKey, noteData).catchError((err) => throw false);
    Fluttertoast.showToast(msg: 'Note Updated');

    return true;
  }

  void setFav(int hiveKey, NoteData noteData) {
    noteData.setFavorite(!noteData.favorite);
    this.box.put(hiveKey, noteData);
  }

  void delete(int hiveKey) {
    this.box.delete(hiveKey);
    Fluttertoast.showToast(msg: 'Note Deleted!');
  }
}
