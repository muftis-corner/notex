// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'checklistGroup.dart';

// **************************************************************************
// TypeAdapterGenerator
// **************************************************************************

class ChecklistGroupDataAdapter extends TypeAdapter<ChecklistGroupData> {
  @override
  final int typeId = 4;

  @override
  ChecklistGroupData read(BinaryReader reader) {
    final numOfFields = reader.readByte();
    final fields = <int, dynamic>{
      for (int i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return ChecklistGroupData(
      fields[0] as String,
      (fields[1] as List).cast<ChecklistData>(),
      fields[2] as DateTime,
      favorite: fields[3] as bool,
      isDeleted: fields[4] as bool,
    );
  }

  @override
  void write(BinaryWriter writer, ChecklistGroupData obj) {
    writer
      ..writeByte(5)
      ..writeByte(0)
      ..write(obj.title)
      ..writeByte(1)
      ..write(obj.items)
      ..writeByte(2)
      ..write(obj.modifiedAt)
      ..writeByte(3)
      ..write(obj.favorite)
      ..writeByte(4)
      ..write(obj.isDeleted);
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is ChecklistGroupDataAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}
