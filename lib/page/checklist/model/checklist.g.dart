// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'checklist.dart';

// **************************************************************************
// TypeAdapterGenerator
// **************************************************************************

class ChecklistDataAdapter extends TypeAdapter<ChecklistData> {
  @override
  final int typeId = 2;

  @override
  ChecklistData read(BinaryReader reader) {
    final numOfFields = reader.readByte();
    final fields = <int, dynamic>{
      for (int i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return ChecklistData(
      fields[0] as String,
      fields[2] as DateTime,
      checked: fields[1] as bool,
      isDeleted: fields[3] as bool,
    );
  }

  @override
  void write(BinaryWriter writer, ChecklistData obj) {
    writer
      ..writeByte(4)
      ..writeByte(0)
      ..write(obj.content)
      ..writeByte(1)
      ..write(obj.checked)
      ..writeByte(2)
      ..write(obj.modifiedAt)
      ..writeByte(3)
      ..write(obj.isDeleted);
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is ChecklistDataAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}
