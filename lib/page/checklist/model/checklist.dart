import 'package:notes/dependencies.dart';

part 'checklist.g.dart';

@HiveType(typeId: 2)
class ChecklistData extends HiveObject {
  @HiveField(0)
  String content;
  @HiveField(1)
  bool checked;
  @HiveField(2)
  DateTime modifiedAt;
  @HiveField(3)
  bool isDeleted;

  ChecklistData(
    this.content,
    this.modifiedAt, {
    this.checked = false,
    this.isDeleted = false,
  });

  void setCheck(bool value) {
    this.checked = value;
  }

  void setContent(String value) {
    this.content = value;
  }

  void setModified() {
    this.modifiedAt = new DateTime.now();
  }

  void softDelete() {
    this.isDeleted = true;
  }

  void undelete() {
    this.isDeleted = false;
  }
}
