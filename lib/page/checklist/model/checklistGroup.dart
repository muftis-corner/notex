import 'package:notes/page/checklist/model/checklist.dart';

import '../../../dependencies.dart';

part 'checklistGroup.g.dart';

@HiveType(typeId: 4)
class ChecklistGroupData extends HiveObject {
  @HiveField(0)
  String title;

  @HiveField(1)
  List<ChecklistData> items;

  @HiveField(2)
  DateTime modifiedAt;

  @HiveField(3)
  bool favorite;

  @HiveField(4)
  bool isDeleted;

  void setFavorite(bool value) {
    this.favorite = value;
  }

  void setNewDate() {
    this.modifiedAt = new DateTime.now();
  }

  void setTitle(String value) {
    this.title = value;
  }

  void softDelete() {
    this.isDeleted = true;
  }

  void undelete() {
    this.isDeleted = false;
  }

  ChecklistGroupData(
    this.title,
    this.items,
    this.modifiedAt, {
    this.favorite = false,
    this.isDeleted = false,
  });
}
