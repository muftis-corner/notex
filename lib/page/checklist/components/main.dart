import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:notes/components/dialog/confirmation.dart';
import 'package:notes/page/checklist/model/checklist.dart';
import '../../../dependencies.dart';
import '../model/checklistGroup.dart';

class ChecklistView extends StatefulWidget {
  final int? hiveKey;
  final ChecklistGroupData? data;
  const ChecklistView({Key? key, this.hiveKey, this.data}) : super(key: key);

  @override
  _ChecklistViewState createState() => _ChecklistViewState();
}

class _ChecklistViewState extends State<ChecklistView> {
  ChecklistGroupData data = ChecklistGroupData('', [], DateTime.now());
  TextEditingController title = TextEditingController();
  Box<ChecklistGroupData> box = Hive.box(HiveBox.checklist);
  var items;

  @override
  void initState() {
    super.initState();
    items = data.items;
    if (widget.data != null) {
      data = widget.data!;
    }
    title.text = data.title;
  }

  @override
  void dispose() {
    title.dispose();
    super.dispose();
  }

  Future<bool> back() async {
    ChecklistGroupData result;
    StreamManager.uiSubject[StreamKey.bottomNavbar]!.add(1);
    if (widget.hiveKey == null) {
      result = new ChecklistGroupData(title.text, data.items, DateTime.now());
      box.add(result);
      Fluttertoast.showToast(msg: 'Checklist Created');
      Navigator.pop(context);
    } else if (widget.hiveKey != null &&
        (data.title != title.text || items != data.items)) {
      result = new ChecklistGroupData(title.text, data.items, DateTime.now());
      box.put(widget.hiveKey, result);
      Fluttertoast.showToast(msg: 'Checklist Updated');
      Navigator.pop(context, result);
    } else {
      Navigator.pop(context);
    }
    return true;
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: back,
      child: Scaffold(
        appBar: AppBar(
          title: TextField(
            autofocus: widget.hiveKey != null ? false : true,
            controller: title,
            style: TextStyle(fontSize: 24, color: Colors.white),
            decoration:
                InputDecoration(hintText: 'Untitled', border: InputBorder.none),
          ),
          actions: [
            IconButton(
                onPressed: () async {
                  if (await showDialog(
                      context: context,
                      builder: (context) => ConfirmDialog(
                            title: 'Delete Checklist ?',
                          ))) {
                    box.delete(widget.hiveKey);
                    Navigator.pop(context);
                  }
                },
                icon: Icon(Ionicons.trash_outline))
          ],
        ),
        floatingActionButton: FloatingActionButton(
          child: Icon(Ionicons.add),
          onPressed: () async {
            String value = await showDialog(
                context: context,
                builder: (context) {
                  return InputChecklist();
                }).then((value) {
              if (value != null) {
                return value;
              }
              return '';
            });
            setState(() {
              if (value.length > 0)
                data.items.add(ChecklistData(value, DateTime.now()));
            });
          },
        ),
        body: ListView.builder(
          itemCount: data.items.length,
          itemBuilder: (context, index) => ChecklistItem(
            index,
            data.items[index],
            deleteCallback: () {
              setState(() {
                data.items.removeAt(index);
              });
            },
          ),
        ),
      ),
    );
  }
}

class ChecklistItem extends StatefulWidget {
  final ChecklistData data;
  final int hiveKey;
  final VoidCallback? deleteCallback;
  const ChecklistItem(
    this.hiveKey,
    this.data, {
    this.deleteCallback,
    Key? key,
  }) : super(key: key);

  @override
  ChecklistItemState createState() => ChecklistItemState();
}

class ChecklistItemState extends State<ChecklistItem> {
  @override
  Widget build(BuildContext context) {
    ChecklistData data = widget.data;

    return ListTile(
      onLongPress: () async {
        var value;
        value = await showDialog(
            barrierDismissible: false,
            context: context,
            builder: (context) {
              return InputChecklist(
                data: data,
                hiveKey: widget.hiveKey,
                deleteCallback: widget.deleteCallback,
              );
            }).then((value) {
          if (value != null) {
            return value;
          }
          return data.content;
        });

        if (data.content != value) {
          setState(() {
            data.content = value;
          });
        }
      },
      onTap: () {
        setState(() {
          data.setCheck(!data.checked);
        });
      },
      leading: Checkbox(
          shape: CircleBorder(),
          value: data.checked,
          onChanged: (value) {
            setState(() {
              data.setCheck(value!);
            });
          }),
      title: Text(
        data.content,
        style: TextStyle(
            fontSize: 18,
            decoration: data.checked
                ? TextDecoration.lineThrough
                : TextDecoration.none),
      ),
    );
  }
}

class InputChecklist extends StatefulWidget {
  final int? hiveKey;
  final ChecklistData? data;
  final VoidCallback? deleteCallback;
  const InputChecklist({Key? key, this.data, this.hiveKey, this.deleteCallback})
      : super(key: key);

  @override
  _InputChecklistState createState() => _InputChecklistState();
}

class _InputChecklistState extends State<InputChecklist> {
  TextEditingController input = TextEditingController();

  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    input.dispose();

    super.dispose();
  }

  List<Widget> createBtn() {
    return [
      Center(
        child: ElevatedButton.icon(
            onPressed: () {
              Navigator.pop(context, input.text);
            },
            icon: Icon(Ionicons.add),
            label: Text('Add')),
      )
    ];
  }

  List<Widget> updateBtn() {
    return [
      ElevatedButton.icon(
          onPressed: () {
            Navigator.pop(context, input.text);
          },
          icon: Icon(Ionicons.save_outline),
          label: Text('Save')),
      ElevatedButton.icon(
          onPressed: () {
            widget.deleteCallback!();
            Navigator.pop(context);
          },
          icon: Icon(Ionicons.trash_outline),
          label: Text('Delete'))
    ];
  }

  @override
  Widget build(BuildContext context) {
    if (widget.data != null) {
      input.text = widget.data!.content;
    }

    return AlertDialog(
      contentPadding: EdgeInsets.only(left: 8, right: 8, top: 8),
      titlePadding: EdgeInsets.only(top: 12),
      actions: [
        Center(
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: widget.data == null ? createBtn() : updateBtn(),
          ),
        )
      ],
      title: Center(
          child: Text(
        widget.data != null ? 'Edit item' : 'Create item',
        style: TextStyle(fontSize: 16),
      )),
      content: CupertinoTextField(
        controller: input,
        autofocus: true,
        placeholder: "Write your needs...",
        maxLines: 1,
        maxLength: 30,
        textInputAction: TextInputAction.done,
        onSubmitted: (value) {
          Navigator.pop(context, value);
        },
        decoration: BoxDecoration(
          border: Border.all(color: Colors.grey),
          borderRadius: BorderRadius.circular(5),
        ),
      ),
    );
  }
}

class ChecklistDummy {
  static List<ChecklistData> data = [
    ChecklistData('test', DateTime.now()),
    ChecklistData('test', DateTime.now()),
    ChecklistData('test', DateTime.now()),
    ChecklistData('test', DateTime.now()),
    ChecklistData('test', DateTime.now()),
    ChecklistData('test', DateTime.now()),
  ];
  static List<ChecklistGroupData> groupData = [
    ChecklistGroupData('Title', data, DateTime.now()),
    ChecklistGroupData('Title', data, DateTime.now()),
    ChecklistGroupData('Title', data, DateTime.now()),
    ChecklistGroupData('Title', data, DateTime.now()),
    ChecklistGroupData('Title', data, DateTime.now()),
    ChecklistGroupData('Title', data, DateTime.now()),
  ];
}
