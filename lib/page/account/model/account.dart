import 'package:notes/dependencies.dart';

part 'account.g.dart';

@HiveType(typeId: 3)
class AccountData extends HiveObject {
  @HiveField(0)
  String title;
  @HiveField(1)
  String account;
  @HiveField(2)
  String password;
  @HiveField(3)
  DateTime modifiedAt;
  @HiveField(4)
  bool favorite;
  @HiveField(5)
  bool isDeleted;

  AccountData(
    this.title,
    this.account,
    this.password,
    this.modifiedAt, {
    this.favorite = false,
    this.isDeleted = false,
  });

  void setTitle(String value) {
    this.title = value;
  }

  void setAccount(String value) {
    this.account = value;
  }

  void setPassword(String value) {
    this.password = value;
  }

  void setNewDate() {
    this.modifiedAt = new DateTime.now();
  }

  void setFavorite() {
    this.favorite = !this.favorite;
  }

  void softDelete() {
    this.isDeleted = true;
  }

  void undelete() {
    this.isDeleted = false;
  }
}
