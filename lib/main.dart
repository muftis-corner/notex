import 'dart:io';

import 'package:flutter/material.dart';
import 'package:notes/hive/hiveHelper.dart';
import 'package:notes/page/checklist/components/main.dart';
import 'dependencies.dart';
import 'page/dashboard/dashboard.dart';
import 'package:flutter/foundation.dart' show kIsWeb;

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  if (!kIsWeb) {
    await Hive.initFlutter();
  }

  StreamManager.uiSubject
      .putIfAbsent(StreamKey.bottomNavbar, () => new BehaviorSubject<int>());

  HiveHelper.registerAdapter();
  await HiveHelper.openAllBox();
  runApp(MyApp());
}

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  @override
  void initState() {
    super.initState();
    print(ThemeData.light().bottomAppBarTheme);
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      themeMode: ThemeMode.dark,
      initialRoute: Routes.Dashboard,
      routes: Routes.items,
      title: 'NoteX',
      // theme: ThemeData(
      //   primaryColor: Colors.red,
      //   primaryColorDark: Colors.red,
      //   primaryColorLight: Colors.red,
      //   primarySwatch: Colors.red,
      //   prima
      // ),
      // theme: ThemeData().copyWith(
      //     primaryColor: Colors.red,
      //     accentColor: Colors.redAccent,
      //     buttonColor: Colors.redAccent,

      // iconTheme: IconThemeData(color: Colors.black),
      // appBarTheme: AppBarTheme(
      //   backgroundColor: Colors.grey[50],
      //   titleTextStyle: ThemeData.light().appBarTheme.titleTextStyle,
      //   iconTheme: IconThemeData(color: Colors.black),
      //   elevation: 0,
      // )
      // ),
      // home: ChecklistView(),
    );
  }
}

class CustomTheme {
  static ThemeData dark = ThemeData.dark();
  static ThemeData light = ThemeData.light().copyWith(
      appBarTheme: AppBarTheme(
    backgroundColor: Colors.grey[50],
    textTheme: Typography.material2014().black,
    elevation: 0,
  ));
}
