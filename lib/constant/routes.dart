// ignore: non_constant_identifier_names
import 'package:flutter/material.dart';
import 'package:notes/page/checklist/components/main.dart';
import 'package:notes/page/dashboard/components/main.dart';
import 'package:notes/page/notes/notes.dart';

class Routes {
  static const String Dashboard = "/dashboard";
  static const String Note = "/note";
  static const String Checklist = '/checklist';

  static Map<String, Widget Function(BuildContext context)> items = {
    Dashboard: (BuildContext context) => DashboardView(),
    Note: (BuildContext context) => NotesView(),
    Checklist: (BuildContext context) => ChecklistView(),
  };
}
