class Label {
  static String category = "Category";
  static String search = "Search";
  static String add = "Add";
  static String favourites = "Favourites";
  static String list = "List";
  static String note = "Note";
  static String account = "Account";
  static String checkList = "Checklist";
  static String createNew = "Create New";
}
